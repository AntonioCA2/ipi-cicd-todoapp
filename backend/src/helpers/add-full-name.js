
/**
 * Fonction d'exemple, sans autre utilité que de démontrer
 * l'écriture d'un test correspondant (voir test/unit/add-full-name.test.js)
 * 
 * Prend en entrée un objet représentant une personne, avec les propriétés
 * firstName et lastName, et ajoute une propriété fullName.
 * 
 * @param {object} person objet représentant une personne
 * @returns {object} objet représentant une personne, avec une propriété fullName
 */
export default function addFullName(person) {
  return {
    ...person,
    fullName: `${person.firstName} ${person.lastName}`,
  }
}