import castDoneToBoolean from '../../src/helpers/cast-done-to-boolean';

describe('castDoneToBoolean', () => {
  it('should convert an object to boolean', () => {
    // ARRANGE
    const item = {
        id: 1, 
        name: "Écrire des tests", 
        done: 0, 
        createdAt: "2024-03-29T10:05", 
        updatedAt: "2024-03-29T10:05"
    };

    // ACT
    const result = castDoneToBoolean(item);
    

    // ASSERT
    expect(result).toEqual({
        id: 1, 
        name: "Écrire des tests", 
        done: false, 
        createdAt: "2024-03-29T10:05", 
        updatedAt: "2024-03-29T10:05"
      });
  });
});
